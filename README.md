# E2_MA_Tim06_2024

Students:
1. Anja Ducic RA 5/2020
2. Ivana Kovacevic RA 15/2020
3. Milica Vujic RA 8/2020
4. Anja Lovric RA 13/2020

## Event organizer mobile app - for Android 11+
Roles: admin, organizer, employee, owner.
Companies can sell products and services, organizers can create events and find everything for it using this app!

## Getting started
To build and run your app, follow these steps:

In the toolbar, select your app from the run configurations menu.
In the target device menu, select the device that you want to run your app on.
If you don't have any devices configured, you need to either create an Android Virtual Device to use the Android Emulator or connect a physical device.
Click Run.

Android Studio warns you if you attempt to launch your project to a device that has an error or a warning associated with it. Iconography and stylistic changes differentiate between errors (device selections that result in a broken configuration) and warnings (device selections that might result in unexpected behavior but are still runnable).

