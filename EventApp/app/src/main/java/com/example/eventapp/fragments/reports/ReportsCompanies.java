package com.example.eventapp.fragments.reports;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventapp.R;
import com.example.eventapp.fragments.FragmentTransition;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReportsCompanies#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportsCompanies extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ReportsCompanies() {
        // Required empty public constructor
    }

    public static ReportsCompanies newInstance() {
        ReportsCompanies fragment = new ReportsCompanies();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentTransition.to(ReportsListCompanies.newInstance(), getActivity(),
                false, R.id.scroll_reports_companies_list);
        return inflater.inflate(R.layout.fragment_reports_companies, container, false);
    }
}