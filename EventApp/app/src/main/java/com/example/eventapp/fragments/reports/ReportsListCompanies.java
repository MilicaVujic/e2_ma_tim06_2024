package com.example.eventapp.fragments.reports;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventapp.R;
import com.example.eventapp.adapters.ReportsAdapter;
import com.example.eventapp.model.Report;
import com.example.eventapp.repositories.ReportRepo;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReportsListCompanies#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReportsListCompanies extends ListFragment {

    private ReportRepo reportRepo;
    private ReportsAdapter adapter;


    public ReportsListCompanies() {
        // Required empty public constructor
    }


    public static ReportsListCompanies newInstance() {
        ReportsListCompanies fragment = new ReportsListCompanies();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportRepo=new ReportRepo();
        reportRepo.getAllCompanyReports(new ReportRepo.ReportFetchCallback() {
            @Override
            public void onReportFetch(ArrayList<Report> reports) {
                ReportRepo.ReportFetchCallback.super.onReportFetch(reports);
                adapter=new ReportsAdapter(getActivity(),reports);
                setListAdapter(adapter);

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reports_list_companies, container, false);
    }
}